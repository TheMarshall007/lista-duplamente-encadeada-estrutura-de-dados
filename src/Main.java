import list.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> l = new List();

        System.out.println("List is empty: " + l.isEmpty());

        l.insert(5);
        System.out.println(l.getFirst() + " / " + l.getLast());

        l.insert(6);
        System.out.println(l.getFirst() + " / " + l.getLast());
        System.out.println("Next first" + l.getFirst().getNext());
        System.out.println("Previous last" + l.getLast().getPrevious());

        System.out.println();

        l.insert(8);
        l.showNodoListFirstToLast();
        l.showNodoLisLastToFirst();
        System.out.println("Element 6 in list: " + l.elementInListFirstToLast(6));
        System.out.println("Element 7 in list: " + l.elementInListLastToFirst(7));

        System.out.println();

        l.insert(10);
        l.insert(11);
        l.insert(12);
        System.out.println("Get element amount: " + l.getNodoAmount());
        System.out.println("List is empty: " + l.isEmpty());
        l.showNodoListFirstToLast();

        System.out.println("Remove using first: ");

        l.removeUsingFirstToStart(6);
        l.showNodoListFirstToLast();
        l.removeUsingFirstToStart(5);
        l.showNodoListFirstToLast();

        System.out.println("Remove using last: ");

        l.removeUsingLastToStart(10);
        l.showNodoListFirstToLast();
        l.removeUsingLastToStart(12);
        l.showNodoListFirstToLast();
    }
}
