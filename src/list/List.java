package list;

import java.util.ArrayList;

public class List<T> {

    private Nodo first;
    private Nodo last;
    private int nodoAmount;

    public void insert(T element) {
        if (isEmpty()) {
            this.first = new Nodo(element);
            this.last = this.first;
            nodoAmount++;
        } else {
            Nodo newNodo = new Nodo(element);
            this.last.setNext(newNodo);
            newNodo.setPrevious(this.last);
            this.last = newNodo;
            nodoAmount++;
        }
    }

    public void removeUsingFirstToStart(T element) {
        if (element == this.first.getElement()) {
            this.first = this.first.getNext();
            this.first.setPrevious(null);
            nodoAmount--;
        } else {
            Nodo nodo = this.first.getNext();
            boolean removed = false;
            for (int i = 0; i < nodoAmount; i++) {
                if (element == nodo.getElement()) {
                    Nodo previous = nodo.getPrevious();
                    Nodo next = nodo.getNext();
                    previous.setNext(next);
                    next.setPrevious(previous);
                    nodoAmount--;
                    removed = true;
                }
                nodo = nodo.getNext();
                if(removed){
                     i = nodoAmount;
                }
            }
        }
    }

    public void removeUsingLastToStart(T element) {
        if (element == last.getElement()) {
            last = last.getPrevious();
            last.setNext(null);
            nodoAmount--;
        } else {
            Nodo nodo = last.getPrevious();
            boolean removed = false;
            for(int i = nodoAmount; i > 0; i--){
                if(element == nodo.getElement()){
                    Nodo previous = nodo.getPrevious();
                    Nodo next = nodo.getNext();
                    previous.setNext(next);
                    next.setPrevious(previous);
                    nodoAmount--;
                    removed = true;
                }
                nodo = nodo.getPrevious();
                if(removed){
                    i=0;
                }
            }
        }
    }

    public boolean isEmpty() {
        return first == null;
    }

    public Nodo getFirst() {
        return first;
    }

    public Nodo getLast() {
        return last;
    }

    public int getNodoAmount() {
        return nodoAmount;
    }

    public boolean elementInListFirstToLast(T element) {
        Nodo nodo = first;
        for (int i = 0; i < nodoAmount; i++) {
            if (nodo.getElement() == element) {
                return true;
            }
            nodo = nodo.getNext();
        }
        return false;
    }

    public boolean elementInListLastToFirst(T element) {
        Nodo nodo = last;
        for (int i = nodoAmount; i > 0; i--) {
            if (nodo.getElement() == element) {
                return true;
            }
            nodo = nodo.getPrevious();
        }
        return false;
    }

    public void showNodoListFirstToLast() {
        Nodo nodo = first;
        System.out.print("[ ");
        for (int i = 0; i < nodoAmount; i++) {
            System.out.print(nodo.getElement());
            if (i + 1 != nodoAmount) {
                System.out.print(" | ");
            }
            nodo = nodo.getNext();
        }
        System.out.println(" ]");
    }

    public void showNodoLisLastToFirst() {
        Nodo nodo = last;
        System.out.print("[ ");
        for (int i = nodoAmount; i > 0; i--) {
            System.out.print(nodo.getElement());
            if (i - 1 != 0) {
                System.out.print(" | ");
            }
            nodo = nodo.getPrevious();
        }
        System.out.println(" ]");
    }

}